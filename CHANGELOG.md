# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.3] - 2023-10-14

### Fixed
- Fixed off-by-one error when rewriting Cell FRMR subrecords leading to corrupt data

## [0.1.2] - 2022-03-28

### Fixed
- Master plugin names are now null-terminated to better support the TES-CS

## [0.1.1] - 2022-03-28

Added missing data to en-de-mapping.yaml

## [0.1.0] - 2022-03-28

Initial Release
