#[macro_use]
extern crate anyhow;
#[macro_use]
extern crate clap;

use anyhow::{Context, Result};
use clap::App;
use encoding::all::{WINDOWS_1251, WINDOWS_1252};
use encoding::{DecoderTrap, EncoderTrap, Encoding};
use esplugin::{GameId, Plugin, PluginEntry, Record, RecordHeader, Subrecord};
use log::{debug, info, warn};
use nom::number::complete::{le_i32, le_i8, le_u24, le_u32, le_u64};
use nom::sequence::tuple;
use regex::{Captures, Regex};
use serde::Deserialize;
use sha2::Digest;
use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::Path;

#[derive(Deserialize, Hash, PartialEq, Eq, Debug)]
enum CellID {
    Interior(String),
    Exterior { x: i32, y: i32 },
}

type CellRefId = (String, u32);

#[derive(Deserialize, Debug)]
#[serde(untagged)]
enum MappingTarget {
    Subrecord {
        record_type: String,
        sub_type: String,
        #[serde(default)]
        first_only: bool,
    },
    InfoSelectRule {
        select_code: String,
    },
    Script {
        script: String,
        arg: u8,
    },
}

#[derive(Deserialize, Debug)]
#[serde(tag = "type")]
enum Mapping {
    CellRef {
        // Mapping breaks up the references for each plugin
        mapping: HashMap<String, HashMap<u32, CellRefId>>,
    },
    Info {
        // Mapping breaks up the info records for each dialogue topic
        mapping: HashMap<String, HashMap<String, String>>,
    },
    String {
        targets: Vec<MappingTarget>,
        mapping: HashMap<String, String>,
    },
}

#[derive(Deserialize)]
struct PluginMapping {
    source: PluginRef,
    dest: Option<PluginRef>,
}

#[derive(Deserialize)]
struct PluginRef {
    name: String,
    size: u64,
    sha256: String,
}

#[derive(Deserialize)]
struct MappingFile {
    plugins: Vec<PluginMapping>,
    source_lang: String,
    dest_lang: String,
    mappings: Vec<Mapping>,
}

fn subrecords_len(subrecords: &[Subrecord]) -> u32 {
    subrecords.iter().map(|x| x.data().len() + 8).sum::<usize>() as u32
}

fn get_subrecord_string(
    record: &Record,
    sub_type: &[u8],
    encoding: &dyn Encoding,
) -> Option<String> {
    for subrecord in record.subrecords() {
        if subrecord.subrecord_type() == sub_type {
            return decode(encoding, subrecord.data()).ok();
        }
    }
    None
}

fn map_subrecord(
    subrecords: &mut [Subrecord],
    sub_type: &[u8],
    first_only: bool,
    func: impl Fn(&Subrecord) -> Result<Option<Vec<u8>>>,
) -> Result<()> {
    for subrecord in subrecords.iter_mut() {
        if subrecord.subrecord_type() == sub_type {
            if let Some(new_data) = func(subrecord)? {
                *subrecord = Subrecord::new(*subrecord.subrecord_type(), new_data, false);
            }
            if first_only {
                break;
            }
        }
    }
    Ok(())
}

fn decode(encoding: &dyn Encoding, data: &[u8]) -> Result<String> {
    encoding
        .decode(data, DecoderTrap::Strict)
        .map_err(|x| anyhow!("Could not decode string {}", x))
        .map(|x| x.trim_end_matches('\0').to_string())
}

fn encode(encoding: &dyn Encoding, string: &str) -> Result<Vec<u8>> {
    encoding
        .encode(string, EncoderTrap::Strict)
        .map_err(|x| anyhow!("Could not encode string {}", x))
}

fn parse_cellid(data: &[u8], masters: &[String]) -> Result<(u32, usize), anyhow::Error> {
    let (id, master) = if data.len() == 8 {
        // 8 bytes: 4 bytes id, 4 bytes content
        let (idnum, master_index) = map_err(tuple((le_u32, le_i32))(data))?.1;
        (idnum, master_index as usize)
    } else {
        // 4 bytes: 24 bits id, 4 bits content
        let (idnum, master_index) = map_err(tuple((le_u24, le_i8))(data))?.1;
        // If index does not appear to match, assume it's just an identifier.
        if 0 <= master_index && (master_index as usize) < masters.len() {
            (idnum, master_index as usize)
        } else {
            let (_, refnum) = map_err(le_u32(data))?;
            warn!(
                "Master index {} was invalid for reference {}",
                master_index, refnum
            );
            (refnum, master_index as usize)
        }
    };
    Ok((id, master))
}

// FIXME: Also map all references to cells (are there references to dialogue?)
// - Cells may occur as destination cell in (door) cell references (DNAM subrecord)
// - PGRD records will also need to be mapped
// - ANAM subrecord in INFO records refers to cells
// - Select Rule NotCell (BL)
// - Script functions will need to be mapped (may be easiest just to find/replace all string
//      literals matching the mapped keys):
//      - GetPCCell
//      - AIEscortCell
//      - AIFollowCell
//      - CenterOnCell
//      - PositionCell
//      - PlaceItemCell
//      - ShowMap
fn map_record(
    source_encoding: &dyn Encoding,
    dest_encoding: &dyn Encoding,
    record: &Record,
    parent: &Option<&Record>,
    mapping: &MappingFile,
    masters: &[String],
) -> Result<Record> {
    let mut subrecords = record.subrecords().to_vec();

    for mapping in &mapping.mappings {
        match mapping {
            Mapping::CellRef { mapping } => {
                if &record.header_type() == b"CELL" {
                    map_subrecord(&mut subrecords, b"FRMR", false, |subrecord: &Subrecord| {
                        let (id, master_index) = parse_cellid(subrecord.data(), masters)?;
                        if let Some(idmap) = mapping.get(&masters[master_index]) {
                            if let Some((plugin, refnum)) = idmap.get(&id) {
                                // Rewrite identifier to match the mapped data
                                let mut newdata = vec![];
                                let new_master = masters
                                    .iter()
                                    .position(|x| x == &plugin.to_lowercase())
                                    .unwrap_or_else(|| {
                                        panic!(
                                            "Mapping changes master to {}, \
                                         but that plugin was not found in the master list!",
                                            plugin
                                        )
                                    });
                                // If it's a narrow refnum, make sure the output is also narrow
                                if subrecord.data().len() == 4 {
                                    newdata.extend(&(new_master as u8).to_le_bytes());
                                    // Actually need a u24, so we'll truncate the u32
                                    newdata.extend(&(*refnum).to_le_bytes()[..=2].to_vec())
                                } else {
                                    newdata.extend(&(new_master as u32).to_le_bytes());
                                    newdata.extend(&(*refnum).to_le_bytes());
                                }
                                info!(
                                    "Replacing cellref id {}::{} with {}::{}",
                                    masters[master_index], id, plugin, refnum
                                );
                                return Ok(Some(newdata));
                            }
                        }
                        Ok(None)
                    })?;
                }
            }
            Mapping::Info { mapping } => {
                if record.header_type() == *b"INFO" {
                    if let Some(parent) = parent {
                        let mapping = |subrecord: &Subrecord| {
                            let value = decode(source_encoding, subrecord.data())?;
                            let dial = get_subrecord_string(parent, b"NAME", source_encoding)
                                .expect("Could not find NAME in DIAL record");

                            if let Some(dial_mapping) = mapping.get(&dial.to_lowercase()) {
                                if let Some(newvalue) = dial_mapping.get(&value.to_lowercase()) {
                                    info!("Replacing \"{}\" with \"{}\" in INFO", value, newvalue,);
                                    let encoded = encode(dest_encoding, newvalue)?;
                                    Ok(Some(encoded))
                                } else {
                                    debug!("Did not match \"{}\" in INFO", value,);
                                    Ok(None)
                                }
                            } else {
                                Ok(None)
                            }
                        };
                        map_subrecord(&mut subrecords, b"INAM", true, mapping)?;
                        map_subrecord(&mut subrecords, b"NNAM", true, mapping)?;
                        map_subrecord(&mut subrecords, b"PNAM", true, mapping)?;
                    }
                }
            }
            Mapping::String { targets, mapping } => {
                // FIXME: Handle targets together instead of processing the record multiple times
                for target in targets {
                    match target {
                        MappingTarget::Subrecord {
                            record_type,
                            sub_type,
                            first_only,
                        } => {
                            if record.header_type() == record_type.as_bytes() {
                                map_subrecord(
                                    &mut subrecords,
                                    sub_type.as_bytes(),
                                    *first_only,
                                    |subrecord: &Subrecord| {
                                        let value = decode(source_encoding, subrecord.data())?;
                                        if let Some(newvalue) = mapping.get(&value.to_lowercase()) {
                                            info!(
                                                "Replacing \"{}\" with \"{}\" in {}.{}",
                                                value, newvalue, record_type, sub_type
                                            );
                                            let encoded = encode(dest_encoding, newvalue)?;
                                            Ok(Some(encoded))
                                        } else {
                                            debug!(
                                                "Did not match \"{}\" in {}.{}",
                                                value, record_type, sub_type
                                            );
                                            Ok(None)
                                        }
                                    },
                                )?;
                            }
                        }
                        MappingTarget::Script { script, arg } => {
                            let map_script_funcs = |subrecord: &Subrecord| {
                                let script_data = decode(source_encoding, subrecord.data())?;
                                let unused_parameters = if arg > &0 {
                                    "([, ]+[^, ]+){".to_string() + &arg.to_string() + "}"
                                } else {
                                    "".to_string()
                                };
                                let func_call_re = Regex::new(
                                    &(script.clone()
                                        + &unused_parameters
                                        + "[, ]+\"(?P<arg>[^\"]*)\""),
                                )?;
                                let script_data = func_call_re.replace_all(
                                    &script_data,
                                    |captures: &Captures| {
                                        let captured_str = captures.name("arg").unwrap().as_str();
                                        if let Some(newvalue) =
                                            mapping.get(&captured_str.to_lowercase())
                                        {
                                            info!(
                                                "Replacing \"{}\" with \"{}\" in call to {} in script",
                                                captured_str, newvalue, script
                                            );
                                            captures
                                                .get(0)
                                                .unwrap()
                                                .as_str()
                                                .replace(captured_str, newvalue)
                                        } else {
                                            // Don't replace
                                            captures.get(0).unwrap().as_str().to_string()
                                        }
                                    },
                                );
                                Ok(Some(encode(dest_encoding, script_data.as_ref())?))
                            };

                            // Process SCPT records and their SCTX text data
                            // Also process inline BNAM scripts in INFO records
                            if &record.header_type() == b"SCPT" {
                                map_subrecord(&mut subrecords, b"SCTX", true, map_script_funcs)?;
                            } else if &record.header_type() == b"INFO" {
                                map_subrecord(&mut subrecords, b"BNAM", true, map_script_funcs)?;
                            }
                        }
                        MappingTarget::InfoSelectRule { select_code } => {
                            let map_select_rule = |subrecord: &Subrecord| {
                                if String::from_utf8_lossy(&subrecord.data()[1..=3]).as_ref()
                                    == select_code
                                {
                                    let mut newdata = subrecord.data().to_vec();
                                    let key = decode(source_encoding, &newdata[5..])?;
                                    if let Some(value) = mapping.get(&key.to_lowercase()) {
                                        info!(
                                            "Replacing \"{}\" with \"{}\" in INFO select rule",
                                            key, value
                                        );
                                        newdata.truncate(5);
                                        newdata.extend(encode(dest_encoding, value)?);
                                    }
                                    Ok(Some(newdata))
                                } else {
                                    Ok(None)
                                }
                            };

                            if &record.header_type() == b"INFO" {
                                map_subrecord(&mut subrecords, b"SCVR", false, map_select_rule)?;
                            }
                        }
                    }
                }
            }
        }
    }
    return Ok(Record::new(
        RecordHeader::new(
            record.header_type(),
            record.header().flags(),
            None,
            subrecords_len(&subrecords),
        ),
        subrecords,
    ));
}

fn map_err<'a, T>(result: Result<T, nom::Err<nom::error::Error<&'a [u8]>>>) -> Result<T> {
    result.map_err(|x: nom::Err<nom::error::Error<&'a [u8]>>| anyhow!("{:?}", x))
}

fn get_masters(source_encoding: &dyn Encoding, plugin: &Plugin) -> Result<Vec<(String, u64)>> {
    let mut masters = vec![];
    let mut master_name = None;
    for subrecord in plugin.get_header_record().subrecords() {
        match subrecord.subrecord_type() {
            b"MAST" => master_name = Some(decode(source_encoding, subrecord.data())?),
            b"DATA" => {
                let master_size = map_err(le_u64(subrecord.data()))?.1;
                masters.push((
                    master_name.expect("Plugin header DATA before MAST subrecord"),
                    master_size,
                ));
                master_name = None;
            }
            _ => (),
        }
    }
    Ok(masters)
}

fn apply_mapping(mapping: &MappingFile, plugin_path: &str) -> Result<Option<Plugin>> {
    let source_encoding = match mapping.source_lang.as_str() {
        "en" | "fr" | "de" | "es" | "it" => WINDOWS_1252,
        "ru" => WINDOWS_1251,
        x => panic!("Unexpected language {}", x),
    };

    let dest_encoding = match mapping.dest_lang.as_str() {
        "en" | "fr" | "de" | "es" | "it" => WINDOWS_1252,
        "ru" => WINDOWS_1251,
        x => panic!("Unexpected language {}", x),
    };

    let mut plugin = Plugin::new(GameId::Morrowind, Path::new(plugin_path));
    plugin
        .parse_file(esplugin::ParseMode::All)
        .context("While parsing from esm".to_string())?;

    let masters = get_masters(source_encoding, &plugin)?;

    let lowercase_plugin_name = plugin
        .path()
        .file_stem()
        .unwrap()
        .to_str()
        .expect("Plugin filenames must be unicode")
        .to_string()
        .to_lowercase();

    // Make sure that plugin is or targets the appropriate version of Morrowind.esm
    // FIXME: use all plugins this mapping targets
    let has_plugin = !masters.iter().any(|(name, size)| {
        mapping.plugins.iter().any(|plugin| {
            name.to_lowercase() == plugin.source.name.to_lowercase() && size == &plugin.source.size
        })
    });
    if has_plugin {
        let file = File::open(plugin_path)?;
        let file_size = file.metadata().unwrap().len();
        let mut hash = None;

        fn get_hash(file: &File) -> Result<String> {
            let mut hasher = sha2::Sha256::new();
            let mut reader = BufReader::new(file);
            loop {
                let buffer = reader.fill_buf()?;
                let size = buffer.len();
                if size == 0 {
                    break;
                }
                hasher.update(buffer);
                reader.consume(size);
            }
            Ok(format!("{:x}", hasher.finalize()))
        }
        // Note: we ignore the name of the plugin when comparing it against the source
        // because in testing I use different versions of the esms side by side with different
        // names. There should be no significant downside, as the hash is the most reliable way of
        // identifying the plugin anyway, and very few plugins would have the same size, so usually
        // the relatively slow hashing isn't necessary.
        let mut plugin_match = false;
        for plugin in &mapping.plugins {
            if file_size == plugin.source.size {
                let hash = if let Some(hash) = &hash {
                    hash
                } else {
                    hash = Some(get_hash(&file)?);
                    hash.as_ref().unwrap()
                };

                if hash == &plugin.source.sha256 {
                    plugin_match = true;
                }
            }
        }
        if !plugin_match {
            // FIXME: List plugins by name
            eprintln!(
                "Skipping plugin {plugin_path} since it does not match or depend on any of the specified plugins",
            );
            return Ok(None);
        }
    }

    let mut master_names: Vec<_> = masters
        .iter()
        .map(|(name, _)| {
            Path::new(&name.to_lowercase())
                .file_stem()
                .unwrap()
                .to_str()
                .unwrap()
                .to_string()
        })
        .collect();
    master_names.insert(0, lowercase_plugin_name);

    let mut records = vec![];
    // Will be set to the preceeding DIAL record for all INFO records
    let mut parent = None;
    for entry in plugin.get_entries() {
        if let PluginEntry::Record(record) = entry {
            if record.header_type() != *b"INFO" {
                parent = None;
            }
            records.push(map_record(
                source_encoding,
                dest_encoding,
                record,
                &parent,
                mapping,
                &master_names,
            )?);
            if record.header_type() == *b"DIAL" {
                parent = Some(record);
            }
        }
    }

    let basename = plugin
        .path()
        .file_stem()
        .unwrap()
        .to_str()
        .expect("Invalid unicode in the plugin path!")
        .to_string()
        + "_"
        + &mapping.dest_lang
        + "."
        + plugin
            .path()
            .extension()
            .map(|x| {
                x.to_str()
                    .expect("Invalid unicode in the plugin extension!")
            })
            .unwrap_or("");
    let path = if let Some(parent) = plugin.path().parent() {
        parent.join(Path::new(&basename))
    } else {
        Path::new(&basename).to_path_buf()
    };

    let mut header = plugin.get_header_record().clone();
    // Replace dependency plugin name/sizes in header if they appear in the mapping plugins
    let mut subrecords = vec![];
    for subrecord in header.subrecords() {
        match subrecord.subrecord_type() {
            b"MAST" | b"DATA" => (),
            _ => subrecords.push(subrecord.clone()),
        }
    }
    for (index, (master, size)) in masters.iter().enumerate() {
        // Lookup in mapping to see if this master should be mapped to something else
        if let Some(mapped_plugin) = mapping.plugins.iter().find(|plugin| {
            plugin.source.name.to_lowercase() == master.to_lowercase()
                && &plugin.source.size == size
        }) {
            if let Some(mapped_dest) = &mapped_plugin.dest {
                info!(
                    "Replacing master {:?}: {} with {:?}: {}",
                    master, size, mapped_dest.name, mapped_dest.size
                );
                let mut null_terminated_encoding = encode(dest_encoding, &mapped_dest.name)?;
                null_terminated_encoding.push(0);
                subrecords.push(Subrecord::new(*b"MAST", null_terminated_encoding, false));
                subrecords.push(Subrecord::new(
                    *b"DATA",
                    mapped_dest.size.to_le_bytes().to_vec(),
                    false,
                ));
            } else if index < masters.len() - 1 {
                // FIXME: We either need a dummy plugin to include in the masters, or we need to
                // fix all the cellrefs to reference the correct indices.
                panic!(
                    "Skipping plugins which have no corresponding version \
                    in the destination localisation is not yet implemented!"
                );
            }
        } else {
            let mut null_terminated_encoding = encode(dest_encoding, master)?;
            null_terminated_encoding.push(0);
            subrecords.push(Subrecord::new(*b"MAST", null_terminated_encoding, false));
            subrecords.push(Subrecord::new(
                *b"DATA",
                (*size).to_le_bytes().to_vec(),
                false,
            ));
        }
    }
    header = Record::new(
        RecordHeader::new(
            header.header_type(),
            header.header().flags(),
            None,
            subrecords_len(&subrecords),
        ),
        subrecords,
    );

    Ok(Some(Plugin::new_with_contents(
        GameId::Morrowind,
        &path,
        header,
        records.into_iter().map(PluginEntry::Record).collect(),
    )))
}

fn main() -> Result<()> {
    let matches = App::new(crate_name!())
        .version("0.1.0")
        .author(crate_authors!())
        .about(crate_description!())
        .arg_from_usage("<mapping_file> 'Mapping file to apply. It must be encoded in yaml (or, by extension, json)'")
        .arg_from_usage("[FILE] ... 'Plugins to be mapped'")
        .arg_from_usage("--verbose 'Displays changes made to plugins'")
        .get_matches();

    let verbosity = if matches.is_present("verbose") { 2 } else { 1 };

    stderrlog::new()
        .module(module_path!())
        .verbosity(verbosity as usize)
        .init()
        .unwrap();

    if let Some(plugins) = matches.values_of("FILE") {
        let mapping_file = File::open(matches.value_of("mapping_file").unwrap())?;
        let reader = BufReader::new(mapping_file);
        let mut mapping: MappingFile = serde_yaml::from_reader(reader)?;
        // Lowercase all the mappings so that we can do a case-insensitive comparison
        for mapping in mapping.mappings.iter_mut() {
            match mapping {
                Mapping::String { mapping, .. } => {
                    let values: Vec<(_, _)> = mapping.drain().collect();
                    for (key, value) in values {
                        mapping.insert(key.to_lowercase(), value);
                    }
                }
                Mapping::Info { mapping } => {
                    let values: Vec<(_, _)> = mapping.drain().collect();
                    for (key, value) in values {
                        mapping.insert(key.to_lowercase(), value);
                    }
                }
                Mapping::CellRef { mapping } => {
                    let values: Vec<(_, _)> = mapping.drain().collect();
                    for (key, value) in values {
                        mapping.insert(key.to_lowercase(), value);
                    }
                }
            }
        }
        for filename in plugins {
            if let Some(newplugin) = apply_mapping(&mapping, filename)? {
                eprintln!("Writing plugin {}...", newplugin.path().display());
                newplugin.write()?;
            }
        }
    }
    Ok(())
}
