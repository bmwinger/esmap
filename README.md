# ESMap

A tool for mapping data in esm/esp/omwaddon/omwgame files. Designed to allow plugins written targetting one localisation of the base Morrowind game data to be mapped to support a different localisation of Morrowind.

```
esmap 0.1.0
Benjamin Winger <bmw@disroot.org>
Tool to map identifiers in ESP/ESM files

USAGE:
    esmap [FLAGS] <mapping_file> [FILE]...

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information
        --verbose    Displays changes made to plugins

ARGS:
    <mapping_file>    Mapping file to apply. It must be encoded in yaml (or, by extension, json)
    <FILE>...         Plugins to be mapped
```

Note: Most of the differences between the base language files can be handled by this identifier mapping system, however there are various minor differences between the versions which are not related to identifier mismatches (primarily extra or missing cell references).

## Usage

E.g. `esmap mappings/en-fr-mapping.yaml <EnglishMod>.esm` will produce `<EnglishMod>_fr.esm`, a version of `<EnglishMod>.esm` with identifiers compatible with the French Morrowind data files.

## Mappings

mappings can be found in the mappings directory.

Note: there are a few irreconcileable differences. Also note that there are various records which are unique to the each version, and missing from each version, though these are generally few in number. No attempt has been made to introduce missing data or remove extra data.

Complete:
- fr-en-mapping.yaml
- en-fr-mapping.yaml
- de-en-mapping.yaml
- en-de-mapping.yaml

Incomplete:
- ru-en-mapping.yaml
- en-ru-mapping.yaml

## Releases

Source code and release binaries can be found on the [Releases Page](https://gitlab.com/bmwinger/esmap/-/releases).
